package proyectomate;

import java.awt.Color;
import java.awt.Graphics2D;

public class Grafica {

    private int a;
    private int b;
    private int c;
    private int x;
    private int y;

    public Grafica() {
    }

    public Grafica(int a, int b, int c, int x, int y) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g) {
        
        //fondo
        g.setColor(Color.lightGray);
        g.fillRect(0 , 0, 800, 800);
        //Plano Cartesiano 
        g.setColor(Color.GRAY);
        
        // eje Y
        g.drawLine(399, 50, 399, 750);
        // eje X
        g.drawLine(50, 399, 750, 399);

    }

    /**
     * si el valor de A es menor que 0
     * @param a valor de A
     * @return retorna True si es concava hacia abajo!
     */
    public boolean concavidad(int a) {
        return (a < 0);
    }

    
    public String ejeSimetria(int a, int b) {
        return "" + -(b) / (2 * a);
    }

    public String discriminante(int a, int b, int c) {
        int res = (b * b) - 4 * a * c;
        return "" + res;
    }

    public String ambito(int a, int b, int c) {
        int res = - Integer.parseInt(discriminante(a, b, c)) / 4 * a;
        return "" + res;
    }
    public String vertice(int a, int b, int c){
        int eje = Integer.parseInt(ejeSimetria(a, b));
        int dis = Integer.parseInt(discriminante(a, b, c));
//        String res = "(- " + b + " / 2 * " + a + " ) , ( - " + dis + " / 4 *" + a + " )";
        return " Resultado : (" + eje + "," + (-dis / (4 * a)) + ")";
    }
    public String rango(int c, int a, int b) {
        int variable = ((-Integer.parseInt(discriminante(a, b, c))) / (4 * a));
        if (concavidad(a)) {
            return "( - ∞ , " + variable + " ]";
        } else {
            return "[ " + variable + " , + ∞ )";
        }
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
